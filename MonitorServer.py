"""
#-------------------------------------------------------------------------------
# url:        MonitorServer
# Purpose:     Function for monitoring an url
#
# Input:       url: the url for the server [str]
#              path: directory of the output logfile [str]
#
# Output:      Logfile containing timestamp, Http status code of the server and
#              a description of the http code. Logfile only made if url is not
#              accessible.
#
# Author:      Simon Makwarth, Miljøstyrelsen
# Created:     11-04-2018
#-------------------------------------------------------------------------------
"""

import time
import datetime
import os
import requests

def MonitorServer_func(url, path):
    #Monitoring server from url
    #Generate timestamps
    time_ts = time.time()
    date_st = datetime.datetime.fromtimestamp(time_ts).strftime('%Y-%m-%d %H:%M:%S')
    date_date = datetime.datetime.fromtimestamp(time_ts).strftime('%Y_%m_%d')

    # Sort http status code and define status-description
    request = requests.get(url)
    if request.status_code in range(200, 300):
        status = date_st+'\tSuccess responses'+'\tHTTP status code:'+str(request.status_code)+'\t'+url
    elif request.status_code in range(100, 200):
        status = date_st+'\tInformational responses'+'\tHTTP status code:'+str(request.status_code)+'\t'+url
    elif request.status_code in range(300, 400):
        status = date_st+'\tRedirection response'+'\tHTTP status code:'+str(request.status_code)+'\t'+url
    elif request.status_code in range(400, 500):
        status = date_st+'\tClient error'+'\tHTTP status code:'+str(request.status_code)+'\t'+url
    elif request.status_code in range(500, 600):
        status = date_st+'\tServer error'+'\tHTTP status code:'+str(request.status_code)+'\t'+url
    else:
        status = 'Error'
    print(status)
  # Write logfile with error if they exist
    f_n = path+r'Logbog_NotOk.txt'
    if request.status_code != 200: # Printing status to log if notconnected to server
        if os.path.exists(f_n):
            log = open(f_n, "a")
            log.write(status+'\n')
            log.close()
        else:
            log = open(f_n, "w")
            log.write(status+'\n')
            log.close()
    else: # Printing status to log if connected to server
        fn_ok = path+r'Logbog_AllOk.txt'
        if os.path.exists(fn_ok):
            log = open(fn_ok, "a")
            log.write(status+'\n')
            log.close()
        else:
            log = open(fn_ok, "w")
            log.write(status+'\n')
            log.close()
