"""
#-------------------------------------------------------------------------------
# Name:        MoniterGeus
# Purpose:     Script running function MonitorServer to monitor GEUS' serverstatus
#
# Input:       none
#
# Output:      none
#
# Author:      Simon Makwarth, Miljøstyrelsen
# Created:     11-04-2018
#-------------------------------------------------------------------------------
"""
import MonitorServer # MST script

###################### EDIT HERE ######################
# Directory for the logfile
DIR = r'H:\CheckGeusServer\logs/'

# URL of the servers
URL = [
    'http://data.geus.dk/geusmap/?mapname=gerda',
    'http://data.geus.dk/geusmap/?mapname=jupiter',
    'http://data.geus.dk/geusmap/?mapname=rapportdb',
    'http://data.geus.dk/geusmap/?mapname=modeldb',
    'https://services.kortforsyningen.dk',
    'http://miljoegis.mim.dk/cbkort?&profile=grundvand',
    'http://gko.geocloud.dk/Welcome',
    'http://geovejledning.dk/',
    'http://www.klimatilpasning.dk/vaerktoejer/grundvand/grundvandskort.aspx',
    'http://data.geus.dk/geusmap/?mapname=grundvand#baslay=baseMapDa&optlay=&extent=88170,5972960,1294830,6553040&layers=mc_grp_analyse&filter_0=dgu_nr%3D%26stofgruppe.num%3D50%26maengde.min%3D%26dato_seneste_analyse.part%3D%26boringsanvendelse.part%3D%26stof_tekst.part%3D',
    'http://data.geus.dk/geusmap/index.jsp?mapname=nitrat_2mg_and_above_aggr#baslay=baseMapDa&optlay=&extent=136720,5900910,1192280,6408350&layers=nitrat_2mg_and_above_aggr5km,nitrat_2mg_and_above'
    ]

#######################################################

#Test url's
#for i in range(len(URL)):
for i, j in enumerate(URL):
    status = MonitorServer.MonitorServer_func(j, DIR)
print('Monitor Geus: done')
